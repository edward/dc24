---
name: About DebConf
---
# About DebConf

DebConf is the annual conference for [Debian](http://debian.org) contributors
and users interested in [improving Debian](https://www.debian.org/intro/help).
[Previous Debian conferences](https://www.debconf.org) have featured speakers
and attendees from all around the world. The last DebConf, [DebConf23][], took
place in Kochi, India and was attended by 328 participants from over
40 countries.

[DebConf23]: https://debconf23.debconf.org/

**DebConf24 is taking place in Busan, South Korea from July 28 to August 04,
2024.**

It is being preceded by DebCamp, from July 21 to July 27, 2024.

<form method="POST" action="https://lists.debian.org/cgi-bin/subscribe.pl">
  <fieldset>
    <legend>Register to the debconf-announce mailing list</legend>
    <label for="user_email">Your email address:</label>
    <input name="user_email" size="40" value="" type="Text">
    <input type="hidden" name="list" value="debconf-announce">
    <input name="action" value="Subscribe" type="Submit">
  </fieldset>
</form>

<br />

## Venue

[Pukyong National University](https://www.pknu.ac.kr/eng) is a national
university with the largest engineering college in Republic of Korea
located in the heart of urban Busan.
The area of 36 hectares offers a networking atmosphere which builds on
collaboration and shared resources.
The [venue](../venue/) offers several conference halls and classrooms for
plenaries, presentations, discussions, and informal workshops.
[Accommodation](../accommodation/) for participants is available in the
student's dormitory in the venue.

<br />
## Codes of Conduct and Community Team

Please see the [Code of Conduct](../coc/) page for more information.

We look forward to seeing you in Busan!

---
name: Accommodation
---

# Accommodation

Attendees with an accommodation [bursary](../bursaries/) will be accommodated
at the [Student's dormitory (Sejong-1 gwan), Pukyong National University][dormitory].

[dormitory]: https://www.openstreetmap.org/way/1238066417

<a class="btn btn-info" role="button"
href="https://www.openstreetmap.org/way/1238066417">
  <i class="fa fa-map" aria-hidden="true"></i> Map
</a>

The bedrooms and the bathrooms will be shared between 2 people.

The room Wi-Fi and wired LAN ports will be provided by the accommodation
facilities.

## Self-paid accommodation

DebConf is providing beds for people who want to pay for accommodation at PKNU.

The cost is 21 EUR/night.
If you want a separate room just for yourself, the cost is 42 EUR/night.

If number of rooms is filled, we will share that information here.

##  Hotels

If you are looking for a hotel nearby the venue, here is a list:

- [Kent Hotel Gwangalli by Kensington](https://www.tripadvisor.com/g297884-d10423568)
- [Hotel Aqua Palace](https://www.tripadvisor.com/g297884-d1162841)
- [Homers Hotel](http://www.homershotel.com/view/index.do?SS_SVC_LANG_CODE=ENG)
- [Hotel Central Bay](http://www.centralbay.co.kr/view/index.do?SS_SVC_LANG_CODE=ENG)
- [Hotel 1](https://www.tripadvisor.com/g297884-d14008119)

You may book a room in nearby hotels or hostels in Busan.
If you don't see any availability on their website, we'd suggest
checking booking aggregator websites or contacting the hotel directly.
